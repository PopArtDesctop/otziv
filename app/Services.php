<?php

use Phalcon\Mvc\View;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaData;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Router as Router;
use Phalcon\Mvc\View\Simple as SimpleView;

class Services extends \Base\Services
{
    /**
     * We register the events manager
     */
    protected function initDispatcher()
    {
        $eventsManager = new EventsManager;

        /**
         * Check if the user is allowed to access certain action using the SecurityPlugin
         */
        $eventsManager->attach('dispatch:beforeExecuteRoute', new SecurityPlugin);

        /**
         * Handle exceptions and not-found exceptions using NotFoundPlugin
         */
        $eventsManager->attach('dispatch:beforeException', new NotFoundPlugin);

        $dispatcher = new Dispatcher;
        $dispatcher->setEventsManager($eventsManager);

        return $dispatcher;
    }

    /**
     * Инициируем обработку phtml шаблонов
     */
    protected function initPhtml($view, $di){
        $php = new PhpEngine( $view, $di );
        return $php;
    }


    /**
     * Получаем схему базы данных в которой работаем
     */
    protected function initSchema(){

        return $this->get('config')->database->schema;
    }

    /**
     * The URL component is used to generate all kind of urls in the application
     */
    protected function initUrl()
    {
        $url = new UrlProvider();
        $url->setBaseUri($this->get('config')->application->baseUri);
        return $url;
    }

    protected function initView()
    {
        $view = new View();

        $view->setViewsDir(APP_PATH . $this->get('config')->application->viewsDir);

        $view->registerEngines([
            ".phtml" => "phtml",
            ".volt" => 'volt'
        ]);

        return $view;
    }

    /**
     * Setting up volt
     */
    protected function initSharedVolt($view, $di) {
        $volt = new VoltEngine($view, $di);

        $volt->setOptions([
            "compiledPath" => APP_PATH . "cache/volt/",
            "compileAlways" => true
        ]);

        $compiler = $volt->getCompiler();
        $compiler->addFunction('is_a', 'is_a');

        return $volt;
    }

    /**
     * Database connection is created based in the parameters defined in the configuration file
     */
    protected function initSharedDb(){
        $config = $this->get('config')->get('database')->toArray();

        $dbClass = 'Phalcon\Db\Adapter\Pdo\\' . $config['adapter'];
        unset($config['adapter']);

        return new $dbClass($config);
    }

    protected function initSimpleView(){
        $view = new SimpleView();
        $view->setViewsDir(APP_PATH . $this->get('config')->application->viewsDir );
        $view->registerEngines( [
            ".phtml" => "phtml",
            ".volt" => 'volt'
        ] );

        return $view;
    }


    /**
     * If the configuration specify the use of metadata adapter use it or use memory otherwise
     */
    protected function initModelsMetadata()
    {
        return new MetaData();
    }

    /**
     * Start the session the first time some component request the session service
     */
    protected function initSession()
    {
        $session = new SessionAdapter();
        $session->start();
        return $session;
    }

    /**
     * Register the flash service with custom CSS classes
     */
    protected function initFlash()
    {
        $flash = new FlashSession([
            'error' => 'alert alert-danger',
            'success' => 'alert alert-success',
            'notice' => 'alert alert-info',
            'warning' => 'alert alert-warning'
        ]);
        if (method_exists($flash, 'setAutoescape')) { $flash->setAutoescape(false); }

        return $flash;
    }

    /**
     * Register a user component
     */
    protected function initElements()
    {
        return new Elements();
    }

    protected function initRouter(){
        $router = new Router();
      //  echo "<pre>"; print_r( $router ); die;
        $router->removeExtraSlashes(true);
        $router->setDefaults(array(
            'controller' => 'index',
            'action' => 'index'
        ));
        $router->add(
            '/:controller/:action/:params',
            [
                'controller' => 1,
                'action'     => 2,
                'params'     => 3
            ]
        );
      //  $router->add('/?{controller:[\w\-]+}/?{action:[\w\-]*}/?{params:/?.*}');
        return $router;
    }
}
