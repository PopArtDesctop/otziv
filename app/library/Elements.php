<?php

use Phalcon\Mvc\User\Component;

/**
 * Elements
 *
 * Helps to build UI elements for the application
 */
class Elements extends Component
{

    private static $min = null;

    private $month = [
        1 => 'января',
        2 => 'февраля',
        3 => 'марта',
        4 => 'апреля',
        5 => 'мая',
        6 => 'июня',
        7 => 'июля',
        8 => 'августа',
        9 => 'сентября',
        10 => 'октября',
        11 => 'ноября',
        12 => 'декабря'
    ];

    private $_headerMenu = [
        'navbar-left' => [
            'index' => [
                'caption' => 'Home',
                'action' => 'index'
            ],
            'invoices' => [
                'caption' => 'Invoices',
                'action' => 'index'
            ],
            '__about' => [
                'caption' => 'About',
                'action' => 'index'
            ],
            'contact' => [
                'caption' => 'Contact',
                'action' => 'index'
            ],
        ],
        'navbar-right' => [
            'session' => [
                'caption' => 'Log In/Sign Up',
                'action' => 'index'
            ],
        ]
    ];

    private $_tabs = [
        'Invoices' => [
            'controller' => 'invoices',
            'action' => 'index',
            'any' => false
        ],
        'Companies' => [
            'controller' => 'companies',
            'action' => 'index',
            'any' => true
        ],
        'Products' => [
            'controller' => 'products',
            'action' => 'index',
            'any' => true
        ],
        'Product Types' => [
            'controller' => 'producttypes',
            'action' => 'index',
            'any' => true
        ],
        'Your Profile' => [
            'controller' => 'invoices',
            'action' => 'profile',
            'any' => false
        ]
    ];

    /**
     * Builds header menu with left and right items
     *
     * @return string
     */
//    public function getMenu()
//    {
//        $auth = $this->session->get('auth');
//        if ($auth) {
//            $this->_headerMenu['navbar-right']['session'] = [
//                'caption' => 'Log Out',
//                'action' => 'end'
//            ];
//        } else {
//            unset($this->_headerMenu['navbar-left']['invoices']);
//        }
//
//        $controllerName = $this->view->getControllerName();
//        foreach ($this->_headerMenu as $position => $menu) {
//            echo '<div class="nav-collapse">';
//            echo '<ul class="nav navbar-nav ', $position, '">';
//
//            foreach ($menu as $controller => $option) {
//                if ($controllerName == $controller) {
//                    echo '<li class="active">';
//                } else {
//                    echo '<li>';
//                }
//
//                echo $this->tag->linkTo(
//                    $controller . '/' . $option['action'],
//                    $option['caption']
//                );
//                echo '</li>';
//            }
//
//            echo '</ul>';
//            echo '</div>';
//        }
//    }

    /**
     * Returns menu tabs
     */
    public function getTabs()
    {
        $controllerName = $this->view->getControllerName();
        $actionName = $this->view->getActionName();

        echo '<ul class="nav nav-tabs">';

        foreach ($this->_tabs as $caption => $option) {
            if ($option['controller'] == $controllerName && ($option['action'] == $actionName || $option['any'])) {
                echo '<li class="active">';
            } else {
                echo '<li>';
            }

            echo $this->tag->linkTo(
                $option['controller'] . '/' . $option['action'],
                $caption
            );
            echo '</li>';
        }

        echo '</ul>';
    }

    /**
     * Выбор меню для разных ролей пользователей
     */
    public function getMenu(){

       return User::getInstance( $this->session )->isAuth();
    }

    /**
     * Выбираем минимальный объем файла который можем загрузить из настроек PHP
     */
    private function min_limit_upload(){
        $max_upload = preg_split('/(?<=[0-9])(?=[a-z]+)/i',ini_get('upload_max_filesize'));
        $max_post = preg_split('/(?<=[0-9])(?=[a-z]+)/i', ini_get('post_max_size') );
        $memory_limit = preg_split('/(?<=[0-9])(?=[a-z]+)/i', ini_get('memory_limit') );
        self::$min['min'] = $max_upload[0];
        self::$min['unit'] = $max_upload[1];
        self::$min['bytes'] = 0;
        if( self::$min['min'] > $max_post[0]) { self::$min['min'] = $max_post[0]; self::$min['unit'] = $max_post[1]; }
        if( self::$min['min'] > $memory_limit[0]) { self::$min['min'] = $memory_limit[0]; self::$min['unit'] = $memory_limit[1]; }
        self::$min['min'] = floor(self::$min['min']/2 );
        if( strtoupper( self::$min['unit'] ) == 'K' ) {
            self::$min['bytes'] = self::$min['min'] * 1024;
        }
        if( strtoupper( self::$min['unit'] ) == 'M' ) {
            self::$min['bytes'] = self::$min['min'] * 1024*1024;
        }
        if( strtoupper( self::$min['unit'] ) == 'G' ) {
            self::$min['bytes'] = self::$min['min'] * 1024*1024*2014;
        }

        return self::$min;
    }

    /**
     * Визуально пишем размер в HTML
     */
    public function upload_limit_to_form(){
        if( is_null( self::$min) ) {
            $m = $this->min_limit_upload();
        } else {
            $m = self::$min;
        }

        return $m['min'].$m['unit'];
    }

    /**
     * Размер загружаемого файла в байтах для проверки на JS
     */
    public function byte_upload_limit_to_form() {
        if( is_null( self::$min) ) {
            $m = $this->min_limit_upload();
        } else {
            $m = self::$min;
        }

        return $m['bytes'];
    }


    public function convert_timestamp_from_base_to_year( $t ){
        if( is_null( $t ) ) {

            return '';
        } else {

            $dt = new \DateTime( $t );
            return $dt->format('Y');
        }

    }

    public function get_timer(){

        return time();
    }

    public function convert_timestamp_from_base_to_offer_reply( $t ){
        $dt = new \DateTime( $t );
        $dt_str = $dt->format( 'j' ) . ' ' . $this->month[$dt->format( 'n' )] . ' ' . $dt->format( 'Y' ) . ', ' . $dt->format( 'H:i' );

        return $dt_str;
    }

}
