<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-11
 * Time: 20:58
 */

use Phalcon\Filter;

trait trait_Check_ID_for_User {

    public function valid( $id = null ){

        $filter = new Filter;
        $id = (int)$filter->sanitize($id, ['striptags', 'trim'] );

        if( is_null( $id ) OR $id == ''){

            throw new \Exception( $this->user->getId() . ' found user NULL ', 404);
        } else {

          if( (int) $id !== $this->user->getId() ){


              throw new \Exception( $this->user->getId() . ' !==------ ' . $id, 404 );
          } else {

              return $id;
          }
        }

        return false;
    }
}