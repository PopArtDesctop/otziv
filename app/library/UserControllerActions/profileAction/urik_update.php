<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-13
 * Time: 18:28
 */

namespace UserControllerActions\profileAction;

trait urik_update {

    public function urik_update ($id, $id_profile){
            $dt = new \DateTime( (int) $this->request->getPost('year_foundation_company', ['striptags', 'int', 'trim']));
            $this->db->begin();
            $sql = "UPDATE " . $this->schema . ".profiles_companies
                                                SET  
                                                name=:company_name,
                                                short_desc=:short_desc_company,
                                                year_foundation=:year_foundation_company,
                                                count_staff=:count_staff_company,
                                                full_desc=:full_desc_company
                                            WHERE id = " . $id_profile;

            $s = $this->db->execute($sql, [
                'company_name' => $this->request->getPost('company_name', ['striptags', 'trim']),
                'short_desc_company' => $this->request->getPost('short_desc_company', ['striptags', 'trim']),
                'year_foundation_company' => $dt->format('Y-m-d H:i:s'),
                'count_staff_company' => (int) $this->request->getPost('count_staff_company', ['striptags', 'trim']),
                'full_desc_company' => $this->request->getPost('full_desc_company', ['striptags', 'trim'])
            ]);

            $sql = "SELECT id FROM " . $this->schema . ".role WHERE name =:name_role";

            $s = $this->db->query($sql, [
                'name_role' => $this->request->getPost('name_role')
            ]);
            $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
            $s = $s->fetchAll();

            $sql = "UPDATE " . $this->schema . ".link_users_role SET  id_role=:id_role  WHERE id_user=:id_user";

            $s = $this->db->execute($sql, [
                'id_role' => $s[0]->id,
                'id_user' => $id
            ]);

            $this->db->commit();

            $this->updateAuth( $id );

            $this->flash->success('Данные обновлены');
            $this->response->redirect($this->view->parent_url . $id .'/');
            $this->view->disable();

            return;
    }
}