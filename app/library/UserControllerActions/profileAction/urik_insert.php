<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-13
 * Time: 18:22
 */

namespace UserControllerActions\profileAction;

trait urik_insert {

    public function urik_insert( $id ){
            $dt = new \DateTime( (int) $this->request->getPost('year_foundation_company', ['striptags', 'int', 'trim']));
            $this->db->begin();
            $sql = "INSERT INTO " . $this->schema . ".profiles_companies( 
                                               name, 
                                               short_desc, 
                                               year_foundation, 
                                               count_staff, 
                                               full_desc, 
                                               url_logo, 
                                               url_img_profile, 
                                               phone, 
                                               city, 
                                               address, 
                                               url_web, 
                                               url_profile, 
                                               payment_type
                                              )
                                           VALUES (:company_name,
                                                   :short_desc_company,
                                                   :year_foundation_company,
                                                   :count_staff_company,
                                                   :full_desc_company,
                                                   :url_logo,
                                                   :url_img_profile,
                                                   :phone,
                                                   :city,
                                                   :address,
                                                   :url_web,
                                                   :url_profile,
                                                   :payment_type
                                                  )";

            $s = $this->db->execute($sql, [
                'company_name' => $this->request->getPost('company_name', ['striptags', 'trim']),
                'short_desc_company' => $this->request->getPost('short_desc_company', ['striptags', 'trim']),
                'year_foundation_company' => $dt->format('Y-m-d H:i:s'),
                'count_staff_company' => (int)$this->request->getPost('count_staff_company', ['striptags', 'trim']),
                'full_desc_company' => $this->request->getPost('full_desc_company', ['striptags', 'trim']),
                'url_logo' => null,
                'url_img_profile' => null,
                'phone' => null,
                'city' => null,
                'address' => null,
                'url_web' => null,
                'url_profile' => null,
                'payment_type' => null
            ]);

            $profileId = $this->db->lastInsertId();

            $sql = "INSERT INTO " . $this->schema . ".link_users_profiles_companies
                                             (
                                                 id_users, 
                                                 id_profiles_companies
                                             )
                                            VALUES (:id_users, :id_profiles_companies )";

            $s = $this->db->execute($sql, [
                'id_users' => $id,
                'id_profiles_companies' => $profileId
            ]);

            $sql = "SELECT id FROM " . $this->schema . ".role WHERE name =:name_role";

            $s = $this->db->query($sql, [
                'name_role' => $this->request->getPost('name_role')
            ]);

            $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
            $s = $s->fetchAll();

            $sql = "UPDATE " . $this->schema . ".link_users_role SET  id_role=:id_role  WHERE id_user=:id_user";

            $s = $this->db->execute($sql, [
                'id_role' => $s[0]->id,
                'id_user' => $id
            ]);

            $this->db->commit();

            $this->updateAuth( $id );

            $this->flash->success('Данные добавлены');
            $this->response->redirect($this->view->parent_url . $id . '/');
            $this->view->disable();

            return;
    }
}