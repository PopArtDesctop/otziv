<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-12
 * Time: 14:44
 */


namespace UserControllerActions\profileAction;


trait index {

    public function index_profile($id) {

        $sql = "SELECT USR.id as id_user, 
                           USR.login as login_user,
                           USR.email AS email_user, 
                           ROLE.name AS name_role,
                           PCO.name AS company_name,
                           PCO.short_desc AS short_desc_company,
                           PCO.year_foundation AS year_foundation_company,
                           PCO.count_staff AS count_staff_company,
                           PCO.full_desc AS full_desc_company,
                           PRF.name AS name_user, 
                           PRF.surname AS surname_user, 
                           PRF.short_desc AS short_desc_user, 
                           PRF.year_birth AS year_birth_user, 
                           PRF.full_desc AS full_desc_user,
                           SEL.name AS name_seller,
                           SEL.logo AS logo_seller,
                           SEL.id AS id_seller,
                           PRF.full_desc AS full_desc_user FROM " . $this->schema . ".users AS USR "
            . "LEFT JOIN " . $this->schema . ".link_users_profiles_users AS LINK_PRF ON USR.id = LINK_PRF.id_users "
            . "LEFT JOIN " . $this->schema . ".link_users_role AS LINK_ROLE ON USR.id = LINK_ROLE.id_user "
            . "LEFT JOIN " . $this->schema . ".role AS ROLE ON ROLE.id = LINK_ROLE.id_role "
            . "LEFT JOIN " . $this->schema . ".profiles_users AS PRF ON PRF.id = LINK_PRF.id_profiles "
            . "LEFT JOIN " . $this->schema . ".link_users_profiles_companies AS LINK_PCO ON LINK_PCO.id_users = USR.id "
            . "LEFT JOIN " . $this->schema . ".profiles_companies AS PCO ON PCO.id = LINK_PCO.id_profiles_companies "
            . "LEFT JOIN " . $this->schema . ".link_users_sellers AS LUSS ON LUSS.id_users = USR.id "
            . "LEFT JOIN " . $this->schema . ".sellers AS SEL ON SEL.id = LUSS.id_sellers "
            . "WHERE USR.id = :id_users AND USR.active = 'true'";

        $s = $this->db->query($sql, [
            'id_users' => $id
        ]);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        return $s[0];
    }

}