<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-13
 * Time: 18:13
 */

namespace UserControllerActions\profileAction;

trait fizik_update {

    public function fizik_update ( $id ){

        if( $this->request->getPost('year_birth_user', ['striptags', 'int', 'trim'] ) == '' ) {
            $dt = null;
        } else {
            $dt = new \DateTime( (int) $this->request->getPost('year_birth_user', ['striptags', 'int', 'trim']));
        }
            $s = $this->select_uniq_id_link_users_profiles_users( $id );

            $this->db->begin();

            $sql = "UPDATE " . $this->schema . ".profiles_users AS PRF "
                . " SET  
                       name=:name_user , 
                       surname=:surname_user, 
                       short_desc=:short_desc_user, 
                       full_desc=:full_desc_user, 
                       year_birth=:year_birth_user
                    WHERE PRF.id=:id_user";

            $s = $this->db->execute($sql, [
                'name_user' => $this->request->getPost('name_user', ['striptags', 'trim']),
                'surname_user' => $this->request->getPost('surname_user', ['striptags', 'trim']),
                'short_desc_user' => $this->request->getPost('short_desc_user', ['striptags', 'trim']),
                'full_desc_user' => $this->request->getPost('full_desc_user', ['striptags', 'trim']),
                'year_birth_user' => ( is_null( $dt ) ) ? $dt : $dt->format('Y-m-d H:i:s'),
                'id_user' => $s->id_profile
            ]);

            $sql = "SELECT id FROM " . $this->schema . ".role WHERE name =:name_role";

            $s = $this->db->query($sql, [
                'name_role' => $this->request->getPost('name_role')
            ]);
            $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
            $s = $s->fetchAll();


            $sql = "UPDATE " . $this->schema . ".link_users_role SET  id_role=:id_role  WHERE id_user=:id_user";

            $s = $this->db->execute($sql, [
                'id_role' => $s[0]->id,
                'id_user' => $id
            ]);

            $this->db->commit();

            $this->updateAuth( $id );

            $this->flash->success('Данные обновлены');
            $this->response->redirect($this->view->parent_url . $id . '/');
            $this->view->disable();

            return;
    }

    public function select_uniq_id_link_users_profiles_users( $id ){
        $sql = "SELECT PRF.id as id_profile FROM " . $this->schema . ".users AS USR "
            . "LEFT JOIN " . $this->schema . ".link_users_profiles_users AS LINK_PRF ON USR.id = LINK_PRF.id_users "
            . "LEFT JOIN " . $this->schema . ".profiles_users AS PRF ON PRF.id = LINK_PRF.id_profiles "
            . "WHERE USR.id = :id AND USR.active = 'true'";

        $s = $this->db->query($sql, [ 'id' => $id ]);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        return $s[0];
    }
}