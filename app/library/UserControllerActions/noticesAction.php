<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-12
 * Time: 15:54
 */

namespace UserControllerActions;

trait noticesAction {

    public function notices( $id ){

        $this->db->begin();

        $sql = "DELETE FROM " . $this->schema . ".link_users_notice_email WHERE id_users=:id_users";
        $s = $this->db->query( $sql, [ 'id_users' => $id ] );

        $sql = "INSERT INTO " . $this->schema . ".link_users_notice_email( id_users, id_notice_email, id_notices) "
	           ."VALUES (:id_users, :id_notice_email, :id_notices )";

        foreach ($_REQUEST as $key => $val) {
            if (stristr($key, 'mail_notice') !== FALSE) {
                list($empty, $id_notice )  = explode("-", $key);
                $s = $this->db->execute($sql, [
                    'id_users' => $id,
                    'id_notice_email' => (int) $val,
                    'id_notices' => (int)$id_notice
                ]);
            }
        }

        $sql = "DELETE FROM " . $this->schema . ".link_users_notice_push WHERE id_users=:id_users";
        $s = $this->db->query( $sql, [ 'id_users' => $id ] );

        $sql = "INSERT INTO " . $this->schema . ".link_users_notice_push( id_users, id_notice_push, id_notices) "
            ."VALUES (:id_users, :id_notice_push, :id_notices )";

        foreach ($_REQUEST as $key => $val) {
            if (stristr($key, 'push_notice') !== FALSE) {
                list($empty, $id_notice )  = explode("-", $key);
                $s = $this->db->execute($sql, [
                    'id_users' => $id,
                    'id_notice_push' => (int) $val,
                    'id_notices' => (int)$id_notice
                ]);
            }
        }

        $this->db->commit();

        $this->flash->success('Параметры успешно изменены');
        $this->response->redirect($this->view->parent_url . $id . '/');
        $this->view->disable();
    }

    public function index_notices( $id ){

        $ss = (object)[];
        $sql = "SELECT * FROM " . $this->schema . ".notices";

        $s = $this->db->query( $sql );
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        $ss->list_notice = $s;

        $sql = "SELECT * FROM " . $this->schema . ".notice_email";

        $s = $this->db->query( $sql );
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        $ss->notice_email = $s;

        $sql = "SELECT * FROM " . $this->schema . ".notice_push";

        $s = $this->db->query( $sql );
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        $ss->notice_push = $s;


        $sql = "SELECT 
                      LINK_MAIL.id_notice_email AS id_mail, 
                      LINK_MAIL.id_notices AS id_notice_mail
	             FROM " . $this->schema . ".users AS USR "
               ."INNER JOIN " . $this->schema .".link_users_notice_email AS LINK_MAIL ON LINK_MAIL.id_users = USR.id "
               ."WHERE USR.id=:id_users AND active = 'true'";

        $s = $this->db->query( $sql, [
            'id_users' => $id
        ] );
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();
        $ss->user_properties_mail = $s;

        $sql = "SELECT 
                      LINK_PUSH.id_notice_push AS id_push,
                      LINK_PUSH.id_notices AS id_notice_push
	             FROM " . $this->schema . ".users AS USR "
            ."INNER JOIN " . $this->schema .".link_users_notice_push AS LINK_PUSH ON LINK_PUSH.id_users = USR.id "
            ."WHERE USR.id=:id_users AND active = 'true'";

        $s = $this->db->query( $sql, [
            'id_users' => $id
        ] );
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        $ss->user_properties_push = $s;

        return $ss;
    }
}