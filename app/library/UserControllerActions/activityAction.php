<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-12
 * Time: 15:39
 */

namespace UserControllerActions;

trait activityAction {

    public function activity( $id ) {

        $this->db->begin();

        $sql = "DELETE FROM " . $this->schema . ".link_users_cities WHERE id_users = :id_users";

        $s = $this->db->execute($sql, ['id_users' => $id ]);

        $listcities = json_decode( $this->request->getPost( 'listcities', ['striptags', 'trim']), true );

        $sql = "INSERT INTO " . $this->schema . ".link_users_cities( id_users, id_cities) VALUES (:id_users, :id_cities)";

        foreach( $listcities as $key=>$val ){
            $s = $this->db->execute($sql, [
                'id_users' => $id,
                'id_cities' => $key
            ]);
        }

        $listcategories = json_decode( $this->request->getPost( 'listcategories', ['striptags', 'trim']), true );

        $sql = "DELETE FROM " . $this->schema . ".link_users_categories WHERE id_users = :id_users ";

        $s = $this->db->execute($sql, ['id_users' => $id ]);

        $sql = "INSERT INTO " . $this->schema . ".link_users_categories( id_users, id_categories) VALUES (:id_users, :id_categories)";

        foreach( $listcategories as $key=>$val ){
            $s = $this->db->execute($sql, [
                'id_users' => $id,
                'id_categories' => $key
            ]);
        }

        $this->db->commit();

        $this->flash->success('Данные обновлены');
        $this->response->redirect($this->view->parent_url . $id . '/');
        $this->view->disable();



    }

    public function index_activity ( $id ){
        $ss = (object)[];

        $sql = "SELECT * FROM " . $this->schema . ".cities WHERE active = 'true' ORDER BY range ASC";
        $s = $this->db->query($sql);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $ss->cities = $s->fetchAll();

        $sql = "SELECT * FROM " . $this->schema . ".categories WHERE active='true'";
        $s = $this->db->query($sql);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $ss->categories = $s->fetchAll();

        $sql = "SELECT * FROM " . $this->schema . ".link_users_cities AS LINK_CITY "
                ."LEFT JOIN " . $this->schema . ".cities AS CITY ON  CITY.id = LINK_CITY.id_cities WHERE id_users=" . $id;
        $s = $this->db->query($sql);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $ss->user_cities = $s->fetchAll();

        $sql = "SELECT * FROM " . $this->schema . ".link_users_categories AS LINK_CATE "
                ."LEFT JOIN " . $this->schema . ".categories AS CATE ON  CATE.id = LINK_CATE.id_categories WHERE id_users=" . $id;
        $s = $this->db->query($sql);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $ss->user_categories = $s->fetchAll();

        return $ss;

    }
}