<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 04.01.20
 * Time: 22:53
 */


namespace UserControllerActions;

trait ordersAction {

    public function get_count_orders(){

        $sql = "SELECT count( ORH.id ) AS cnt FROM " . $this->schema . ".orders_hip AS ORH "
            . "LEFT JOIN " . $this->schema . ".link_users_orders_hip AS LUOH ON LUOH.id_orders_hip = ORH.id "
            . "WHERE LUOH.id_users = " . $this->user->getId();
//echo "<pre>"; print_r( $sql ); die;
        $s = $this->db->query( $sql );
        $s->setFetchMode( \Phalcon\Db::FETCH_OBJ );
        $s = $s->fetchAll();

        return $s[0]->cnt;
    }


    public function get_orders_for_user( ){
        $sql = "SELECT    ORH.id AS id_order , 
                          ORH.order_name AS order_name,
                          ORH.email AS order_email,
                          ORH.phone AS order_phone,
                          ORH.file_name AS order_file_name,
                          ORH.active AS order_active,
                          to_char( ORH.date_create, 'HH24:MI') AS tm_create_order, 
                          to_char( ORH.date_create, 'DD.MM.YYYY') AS dt_create_order
                          FROM " . $this->schema . ".orders_hip AS ORH "
            . "LEFT JOIN " . $this->schema . ".link_users_orders_hip AS LUOH ON LUOH.id_orders_hip = ORH.id "
            . "WHERE LUOH.id_users = " . $this->user->getId()
            . " ORDER BY ORH.date_create";

        $s = $this->db->query($sql);
        $s->setFetchMode( \Phalcon\Db::FETCH_OBJ );
        $a['orders'] = [];
        $a['offers'] = [];
        $a['favorite'] = [];
        foreach( $s->fetchAll() as $key=>$value ){
            foreach( $value as $key_val => $val ) {
                $a['orders'][$value->id_order][$key_val] = $val;
            }
        }

        $sql = "SELECT    ORH.id AS id_order,
                          OFH.id AS offers_id,
                          OFH.description AS offers_description,
                          OFH.cost AS offers_cost,
                          OFH.days_for_wait AS offers_days_for_wait,
                          OFH.name AS offers_name,
                          OFH.phone AS offers_phone,
                          OFH.email AS offers_email,
                          OFH.file_name AS offers_file_name,
                          OFH.active AS offers_active,
                          to_char( OFH.date_create, 'HH24:MI') AS tm_create_offers, 
                          to_char( OFH.date_create, 'DD.MM.YYYY') AS dt_create_offers,
                          LOHOH.favorite AS offers_favorite
                          FROM " . $this->schema . ".orders_hip AS ORH "
            . "LEFT JOIN " . $this->schema . ".link_users_orders_hip AS LUOH ON LUOH.id_orders_hip = ORH.id "
            . "LEFT JOIN " . $this->schema . ".link_offers_hip_orders_hip AS LOHOH ON LOHOH.id_orders_hip = ORH.id "
            . "LEFT JOIN " . $this->schema . ".offers_hip AS OFH ON OFH.id = LOHOH.id_offers_hip "
            . "WHERE LUOH.id_users = " . $this->user->getId() . " AND OFH.id IS NOT NULL "
            . "ORDER BY ORH.date_create";

        $s = $this->db->query($sql);
        $s->setFetchMode( \Phalcon\Db::FETCH_OBJ );
        foreach( $s->fetchAll() as $key=>$value ){
            foreach( $value as $key_val => $val ){
                $a['offers'][$value->id_order][$value->offers_id][$key_val] = $val;
                if( $value->offers_favorite == 1 ) {
                    $a['favorite'][$value->id_order]['favorite'] = $val;
                    $a['favorite'][$value->id_order]['offer_id'] = $value->offers_id;
                }
            }
        }

        return $a;
    }

}