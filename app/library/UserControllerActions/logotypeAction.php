<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-12
 * Time: 15:31
 */

namespace UserControllerActions;

trait logotypeAction {

    public function logotype( $id, $id_profile, $old_logo ){

        if ($this->request->isPost()) {

            if ( $this->request->hasFiles(true) == true ) {
                $upload_dir = APP_PATH . $this->config->upload->user_logo_dir;
                $fl = null;
                foreach ($this->request->getUploadedFiles('file_logo_img') as $fl) {
                    $file = $fl;
                }

                if( $file->getError() > 0 ) {
                    $this->flash->error('Ошибка загрузки файла');
                    $this->response->redirect($this->view->parent_url . $id .'/');
                    $this->view->disable();

                    return;
                }

//                if( strtolower($file->getExtension() ) != 'pdf' AND $file->getRealType() != 'application/pdf'){
//                    $this->flash->error( 'Возможна загрузка только pdf файла' );
//                    $this->response->redirect($this->view->parent_url);
//                    $this->view->disable();
//
//                    return;
//                }

                $dt = new \DateTime();

                $name_file = md5 ( $file->getName().$dt->getTimestamp() ) . "." .$file->getExtension();

                if( $fl->moveTo($upload_dir . $name_file )) {
                        $this->db->begin();

                    $sql = "UPDATE " . $this->schema . ".profiles_companies
                                                SET  
                                                url_logo=:url_logo_company
                                            WHERE id = " . $id_profile;

                    $s = $this->db->execute($sql, [
                        'url_logo_company' => $name_file
                    ]);
                        $this->db->commit();

                        unlink($upload_dir . $old_logo );

                        $this->flash->success( 'Логотип сохранен' );
                        $this->response->redirect($this->view->parent_url . $id .'/');
                        $this->view->disable();

                        return;

                } else {
                    $this->flash->error( 'Ошибка сохранения файла заказа' );
                    $this->response->redirect($this->view->parent_url . $id .'/');
                    $this->view->disable();

                    return;
                }

            } else {
                $this->flash->error( 'Вы не загрузили файл с логотипом' );
                $this->response->redirect($this->view->parent_url . $id .'/');
                $this->view->disable();

                return;
            }
        }

    }
}