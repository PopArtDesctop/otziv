<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-12
 * Time: 15:52
 */

namespace UserControllerActions;

trait passwordAction {

    public function password( $id ){
        $old_pass = $this->user->hashPASS( $this->request->getPost('old_pass', ['striptags', 'trim'] ) );
        $new_pass = $this->user->hashPASS( $this->request->getPost('new_pass', ['striptags', 'trim'] ) );
        $double_new_pass = $this->user->hashPASS( $this->request->getPost('new_pass', ['striptags', 'trim'] ) );
        if ( $new_pass === $double_new_pass ){

            $sql = "SELECT count( id ) AS cnt FROM " . $this->schema . ".users WHERE id=:id_users AND password=:old_pass";
            $s = $this->db->query($sql, [
                'id_users' => $id,
                'old_pass' => $old_pass
            ]);
            $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
            $s = $s->fetchAll();

            if( $s[0]->cnt === 1 ){

                $sql = "UPDATE " . $this->schema .".users
	                     SET password=:new_pass
	                    WHERE id=:id_users";
                $s = $this->db->execute($sql, [
                    'new_pass' => $new_pass,
                    'id_users' => $id
                ]);

                $this->flash->success('Пароль успешно изменен');
                $this->response->redirect($this->view->parent_url . $id . '/');
                $this->view->disable();

                return;
            } else {

                $this->flash->success('Старый пароль не правильныый');
                $this->response->redirect($this->view->parent_url . $id . '/');
                $this->view->disable();

                return;
            }

        } else {
            $this->flash->success('Новые пароли не совпадают');
            $this->response->redirect($this->view->parent_url . $id . '/');
            $this->view->disable();

            return;
        }
    }
}