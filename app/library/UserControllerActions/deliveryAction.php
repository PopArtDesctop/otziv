<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-12
 * Time: 15:41
 */

namespace UserControllerActions;

trait deliveryAction
{


    public function delivery($id){

        $this->db->begin();
        if (isset($_REQUEST['payment-type'])) {

            $sql = "DELETE FROM " . $this->schema . ".link_users_payment_types WHERE id_users=:id_users";
            //echo $sql; die;
            $s = $this->db->execute($sql, ['id_users' => $id]);

            $sql = "INSERT INTO dev.link_users_payment_types( id_users, id_payment_types) VALUES ( :id_users, :id_payment_types )";

            $s = $this->db->execute($sql, [
                'id_users' => $id,
                'id_payment_types' => (int)$this->request->getPost('payment-type', ['striptags', 'trim'])
            ]);
        }

        $sql = "DELETE FROM " . $this->schema . ".link_users_delivery_types WHERE id_users=:id_users";
        $s = $this->db->execute($sql, ['id_users' => $id]);

        $sql = "INSERT INTO " . $this->schema . ".link_users_delivery_types( id_users, id_delivery_types) VALUES ( :id_users, :id_delivery_types )";

        foreach ($_REQUEST as $key => $val) {
            if (stristr($key, 'delivery-type') !== FALSE) {
                $s = $this->db->execute($sql, [
                    'id_users' => $id,
                    'id_delivery_types' => (int)$this->request->getPost($key, ['striptags', 'trim'])
                ]);
            }
        }

        $this->db->commit();

        $this->flash->success('Данные обновлены');
        $this->response->redirect($this->view->parent_url . $id . '/');
        $this->view->disable();

    }



    public function index_delivery( $id ){

        $ss = (object)[];
        $sql = "SELECT  PAY.id AS id_pay,
                        PAY.desc AS desc_pay,
                        DET.id AS id_delivery,
                        DET.desc AS desc_delivery 
                  FROM " . $this->schema . ".users AS USR "
                ."LEFT JOIN " . $this->schema . ".link_users_payment_types AS LINK_PAY ON LINK_PAY.id_users = USR.id "
                ."LEFT JOIN " . $this->schema . ".payment_types AS PAY ON PAY.id = LINK_PAY.id_payment_types "
                ."LEFT JOIN " . $this->schema . ".link_users_delivery_types AS LINK_DET ON LINK_DET.id_users = USR.id "
                ."LEFT JOIN " . $this->schema.  ".delivery_types AS DET ON DET.id = LINK_DET.id_delivery_types "
                ."WHERE USR.id = :id_users AND active = 'true'";
//echo $sql; die;
        $s = $this->db->query($sql, [
            'id_users' => $id
        ]);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        $ss->id_pay = $s[0]->id_pay;

        $ss->delivery = $s;

        $sql = "SELECT * FROM " . $this->schema . ".delivery_types AS DET ";

        $s =   $this->db->query( $sql );
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $ss->delivery_types = $s->fetchAll();

//echo "<pre>"; print_r( $ss ); die;
        return $ss;
    }
}