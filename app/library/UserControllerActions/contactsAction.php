<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-12
 * Time: 15:34
 */

namespace UserControllerActions;

trait contactsAction {

    public function contacts_insert( $id ){
       $this->db->begin();

       $sql ="INSERT INTO " . $this->schema . ".addres_phone( phone, city, addres, url_web, url_place)
	          VALUES ( :phone, :city, :addres, :url_web, :url_place )";

        $s = $this->db->execute($sql, [
            'phone' => $this->request->getPost('city', ['striptags', 'trim']),
            'city' => $this->request->getPost('city', ['striptags', 'trim']),
            'addres' => $this->request->getPost('addres', ['striptags', 'trim']),
            'url_web' => $this->request->getPost('url_web', ['striptags', 'trim']),
            'url_place' => $this->request->getPost('url_place', ['striptags', 'trim'])
        ]);

        $profileId = $this->db->lastInsertId();

        echo "<pre>"; var_dump("------", $profileId );

        $sql = "INSERT INTO " . $this->schema . ".link_users_addres_phone
                                             (
                                                 id_users, 
                                                 id_addres_phone
                                             )
                                            VALUES (:id_users, :id_addres_phone )";

        $s = $this->db->execute($sql, [
            'id_users' => $id,
            'id_addres_phone' => $profileId
        ]);

        $sql = "INSERT INTO " . $this->schema . ".link_users_contact_cities( id_users, id_cities) VALUES ( :id_users, :id_cities)";


        $s = $this->db->execute($sql, [
            'id_users' => $id,
            'id_cities' => ( int ) $this->request->getPost('region_id', ['striptags', 'trim'])
        ]);

        $this->db->commit();

        $this->flash->success('Данные добавлены');
        $this->response->redirect($this->view->parent_url . $id . '/');
        $this->view->disable();
    }


    public function contacts_update( $id, $id_address_phone){
        $this->db->begin();
        $sql = "UPDATE " . $this->schema . ".addres_phone
                SET 
                    phone=:phone,
                    city=:city, 
                    addres=:addres, 
                    url_web=:url_web, 
                    url_place=:url_place
                WHERE id = " . $id_address_phone;

        $s = $this->db->execute($sql, [
            'phone' => $this->request->getPost('phone', ['striptags', 'trim']),
            'city' => $this->request->getPost('city', ['striptags', 'trim']),
            'addres' => $this->request->getPost('addres', ['striptags', 'trim']),
            'url_web' => $this->request->getPost('url_web', ['striptags', 'trim']),
            'url_place' => $this->request->getPost('url_place', ['striptags', 'trim'])
        ]);

        $sql = "UPDATE " . $this->schema . ".link_users_contact_cities "
               ."SET  id_cities=:id_cities  WHERE id_users=:id_users";

        $s = $this->db->execute($sql, [
                'id_users' => $id,
                'id_cities' =>(int)$this->request->getPost('region_id', ['striptags', 'trim'])
            ]);

        $this->db->commit();

        $this->flash->success('Данные обновлены');
        $this->response->redirect($this->view->parent_url . $id . '/');
        $this->view->disable();

        return;
    }


    public function index_contact( $id ){
        $sql = " SELECT ADS.phone AS phone,
                       ADS.city AS city,
                       ADS.addres AS addres,
                       ADS.url_web AS url_web,
                       ADS.url_place AS url_place,
                       LINK_CCT.id_cities AS region_id,
                       CITY.name AS region_name
                       FROM " . $this->schema . ".addres_phone AS ADS "
            . "LEFT JOIN " . $this->schema . ".link_users_addres_phone AS LINK_ADP ON ADS.id = LINK_ADP.id_addres_phone "
            . "LEFT JOIN " . $this->schema . ".users AS USR ON LINK_ADP.id_users = USR.id "
            . "LEFT JOIN " . $this->schema . ".link_users_contact_cities AS LINK_CCT ON LINK_CCT.id_users = USR.id "
            . "LEFT JOIN " . $this->schema . ".cities AS CITY ON CITY.id = LINK_CCT.id_cities "
            . "WHERE USR.id = :id AND USR.active = 'true'";


        $s = $this->db->query($sql, [
            'id' => $id
        ]);

        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        return $s[0];
    }
}