<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-12
 * Time: 15:53
 */

namespace UserControllerActions;

trait emailAction {

    public function email( $id ){

        $this->db->begin();

        $old_email = $this->request->getPost('old_email', ['striptags', 'trim'] );
        $new_email = $this->request->getPost('new_email', ['striptags', 'trim'] );

        $sql = "SELECT count( id ) AS cnt FROM " . $this->schema . ".users WHERE id=:id_users AND email=:email";
        $s = $this->db->query($sql, [
            'id_users' => $id,
            'email' =>  $old_email
        ]);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        if( $s[0]->cnt === 1 ){

            $sql = "UPDATE " . $this->schema .".users
	                     SET email=:new_email
	                    WHERE id=:id_users";
            $s = $this->db->execute($sql, [
                'new_email' => $new_email,
                'id_users' => $id
            ]);

            $this->db->commit();

//            $subject = 'Изменена почта';
//            $message = 'Вы произвели смену почтового адреса на ' . $new_email ;
//            $headers = 'From: webmaster@place.ru' . "\r\n" .
//                'Reply-To: webmaster@place.ru' . "\r\n" .
//                'X-Mailer: PHP/' . phpversion();
//            mail($new_email, $subject, $message, $headers);

//            $mail = new \PHPMailer(true);
//            //Server settings
//            $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
//            $mail->isSMTP();                                            // Send using SMTP
//            $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
//            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
//            $mail->Username   = 'massagumi@gmail.com';                     // SMTP username
//            $mail->Password   = 'Grj74^gdg$dff';                               // SMTP password
//            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
//            $mail->Port       = 465;
//
//
//            $mail->setFrom('place@place.ru', 'Mailer');
////            $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
////            $mail->addAddress('ellen@example.com');               // Name is optional
//              $mail->addReplyTo('avtorpc@gmail.com', 'Information');
////            $mail->addCC('cc@example.com');
////            $mail->addBCC('bcc@example.com');
//
//
//            $mail->isHTML(true);                                  // Set email format to HTML
//            $mail->Subject = 'Here is the subject';
//            $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
//            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
//
//            $mail->send();

            $this->flash->success('Почта успешно изменен');
            $this->response->redirect($this->view->parent_url . $id . '/');
            $this->view->disable();

            return;
        } else {

            $this->flash->success('Старая почта не существует');
            $this->response->redirect($this->view->parent_url . $id . '/');
            $this->view->disable();

            return;
        }
    }
}