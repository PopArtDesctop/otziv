<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 07.01.20
 * Time: 15:06
 */

namespace UserControllerActions;

trait offersAction {

    public function get_count_offers( $id_order ){

        $sql = "SELECT count( ORF.id ) AS cnt FROM " . $this->schema . ".offers_hip AS ORF "
            . "LEFT JOIN " . $this->schema .  ".link_offers_hip_orders_hip AS LOFR ON LOFR.id_offers_hip = ORF.id "
            . "LEFT JOIN " . $this->schema . ".link_users_offers_hip AS LUOF ON LUOF.id_offers_hip = ORF.id "
            . "LEFT JOIN " . $this->schema . ".orders_hip AS ORH ON ORH.id = LOFR.id_orders_hip "
            . "WHERE LUOF.id_users = " . $this->user->getId() . " AND ORH.id=:id_order";

        $s = $this->db->query( $sql, ['id_order' => $id_order]);
        $s->setFetchMode( \Phalcon\Db::FETCH_OBJ );
        $s = $s->fetchAll();

        return $s[0]->cnt;
    }

    public function get_user_offers( $id_order ){

        $sql = "SELECT    ORH.order_name AS order_name,
                          LOFR.id_orders_hip AS id_order,
                          LOFR.favorite AS offer_favorite,
                          ORF.id AS id_offer, 
                          ORF.description AS offer_description,
                          ORF.cost AS offres_cost,
                          ORF.days_for_wait AS offer_days_for_wait,
                          ORF.name AS offres_name,
                          ORF.phone AS offer_phone,
                          ORF.email AS offers_email,
                          ORF.file_name AS offer_file_name,
                          ORF.active AS offers_active,
                          to_char( ORF.date_create, 'HH24:MI') AS tm_create_offer, 
                          to_char( ORF.date_create, 'DD.MM.YYYY') AS dt_create_offer
                          FROM " . $this->schema . ".offers_hip AS ORF "
            . "LEFT JOIN " . $this->schema .  ".link_offers_hip_orders_hip AS LOFR ON LOFR.id_offers_hip = ORF.id "
            . "LEFT JOIN " . $this->schema . ".link_users_offers_hip AS LUOF ON LUOF.id_offers_hip = ORF.id "
            . "LEFT JOIN " . $this->schema . ".orders_hip AS ORH ON ORH.id = LOFR.id_orders_hip "
            . "WHERE LUOF.id_users = " . $this->user->getId() . " AND ORH.id=:id_order"
            . " ORDER BY ORF.date_create DESC";

        $s = $this->db->query($sql, ['id_order' => $id_order]);
        $s->setFetchMode( \Phalcon\Db::FETCH_OBJ );
        $a = [];
        foreach( $s->fetchAll() as $key=>$value ){
            foreach( $value as $key_val => $val ) {
                $a[$value->id_offer][$key_val] = $val;
            }
        }

        return $a;
    }

    public function get_count_offers_all(){

        $sql = "SELECT count( ORF.id ) AS cnt FROM " . $this->schema . ".offers_hip AS ORF "
            . "LEFT JOIN " . $this->schema . ".link_users_offers_hip AS LUOF ON LUOF.id_offers_hip = ORF.id "
            . "WHERE LUOF.id_users = " . $this->user->getId();

        $s = $this->db->query( $sql );
        $s->setFetchMode( \Phalcon\Db::FETCH_OBJ );
        $s = $s->fetchAll();

        return $s[0]->cnt;
    }

    public function get_user_offers_all(){

        $sql = "SELECT    ORH.order_name AS order_name,
                          LOFR.id_orders_hip AS id_order,
                          LOFR.favorite AS offer_favorite,
                          ORF.id AS id_offer, 
                          ORF.description AS offer_description,
                          ORF.cost AS offres_cost,
                          ORF.days_for_wait AS offer_days_for_wait,
                          ORF.name AS offres_name,
                          ORF.phone AS offer_phone,
                          ORF.email AS offers_email,
                          ORF.file_name AS offer_file_name,
                          ORF.active AS offers_active,
                          to_char( ORF.date_create, 'HH24:MI') AS tm_create_offer, 
                          to_char( ORF.date_create, 'DD.MM.YYYY') AS dt_create_offer
                          FROM " . $this->schema . ".offers_hip AS ORF "
            . "LEFT JOIN " . $this->schema .  ".link_offers_hip_orders_hip AS LOFR ON LOFR.id_offers_hip = ORF.id "
            . "LEFT JOIN " . $this->schema . ".link_users_offers_hip AS LUOF ON LUOF.id_offers_hip = ORF.id "
            . "LEFT JOIN " . $this->schema . ".orders_hip AS ORH ON ORH.id = LOFR.id_orders_hip "
            . "WHERE LUOF.id_users = " . $this->user->getId()
            . " ORDER BY ORF.date_create DESC";

        $s = $this->db->query($sql);
        $s->setFetchMode( \Phalcon\Db::FETCH_OBJ );
        $a = [];
        foreach( $s->fetchAll() as $key=>$value ){
            foreach( $value as $key_val => $val ) {
                $a[$value->id_offer][$key_val] = $val;
            }
        }

        return $a;
    }
}