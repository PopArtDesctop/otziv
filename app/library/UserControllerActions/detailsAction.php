<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-12
 * Time: 15:50
 */

namespace UserControllerActions;

trait detailsAction {

    public function insert_details( $id ){
        $this->db->begin();

        $sql = "INSERT INTO " . $this->schema . ".payment_details(
	            inn, kpp, ogrn, okpo, okved, settlement_account, correspondent_account, legal_address, director)
	            VALUES (:inn, :kpp, :ogrn, :okpo, :okved, :settlement_account, :correspondent_account, :legal_address, :director)";

        $s = $this->db->execute($sql, [
            'inn' => $this->request->getPost('inn', ['striptags', 'trim']),
            'kpp' => $this->request->getPost('kpp', ['striptags', 'trim']),
            'ogrn' => $this->request->getPost('ogrn', ['striptags', 'trim']),
            'okpo' => $this->request->getPost('okpo', ['striptags', 'trim']),
            'okved' => $this->request->getPost('okved', ['striptags', 'trim']),
            'settlement_account' => $this->request->getPost('settlement_account', ['striptags', 'trim']),
            'correspondent_account' => $this->request->getPost('correspondent_account', ['striptags', 'trim']),
            'legal_address' => $this->request->getPost('legal_address', ['striptags', 'trim']),
            'director' => $this->request->getPost('director', ['striptags', 'trim'])
        ]);

        $profileId = $this->db->lastInsertId();

        $sql = "INSERT INTO " . $this->schema . ".link_users_payments_details
                                             (
                                                 id_users, 
                                                 id_payments_datails
                                             )
                                            VALUES (:id_users, :id_payments_datails )";

        $s = $this->db->execute($sql, [
            'id_users' => $id,
            'id_payments_datails' => $profileId
        ]);

        $this->db->commit();

        $this->flash->success('Данные добавлены');
        $this->response->redirect($this->view->parent_url . $id . '/');
        $this->view->disable();

        return;
    }

    public function update_details( $id, $id_payments_datails ){

        $sql = "UPDATE " . $this->schema . ".payment_details "
	           ."SET inn=:inn,
                     kpp=:kpp,
                     ogrn=:ogrn,
                     okpo=:okpo,
                     okved=:okved,
                     settlement_account=:settlement_account,
                     correspondent_account=:correspondent_account,
                     legal_address=:legal_address,
                     director=:director
	           WHERE id=:id_payments_datails";

        $s = $this->db->execute($sql, [
            'inn' => $this->request->getPost('inn', ['striptags', 'trim']),
            'kpp' => $this->request->getPost('kpp', ['striptags', 'trim']),
            'ogrn' => $this->request->getPost('ogrn', ['striptags', 'trim']),
            'okpo' => $this->request->getPost('okpo', ['striptags', 'trim']),
            'okved' => $this->request->getPost('okved', ['striptags', 'trim']),
            'settlement_account' => $this->request->getPost('settlement_account', ['striptags', 'trim']),
            'correspondent_account' => $this->request->getPost('correspondent_account', ['striptags', 'trim']),
            'legal_address' => $this->request->getPost('legal_address', ['striptags', 'trim']),
            'director' => $this->request->getPost('director', ['striptags', 'trim']),
            'id_payments_datails' => $id_payments_datails
        ]);

        $this->flash->success('Данные обновлены');
        $this->response->redirect($this->view->parent_url . $id . '/');
        $this->view->disable();

        return;
    }

    public function index_details( $id ){

        $sql =  "SELECT PDE.* FROM " . $this->schema .".users AS USR "
                  ."LEFT JOIN " . $this->schema . ".link_users_payments_details AS LINK_PDE ON LINK_PDE.id_users = USR.id "
                  ."LEFT JOIN " . $this->schema . ".payment_details AS PDE ON PDE.id = LINK_PDE.id_payments_datails "
                  ."WHERE USR.id = :id_users AND USR.active = 'true'";

        $s = $this->db->query($sql, [
            'id_users' => $id
        ]);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $s = $s->fetchAll();

        return $s[0];
    }
}