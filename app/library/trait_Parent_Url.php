<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-07
 * Time: 15:19
 */

trait trait_Parent_Url{

    public function getParentUrl(){
        if ( is_null( $this->request->getPost('parent_url') )) {

            if( $this->view->action_name == 'index' ){
                if( $this->view->controller_name == 'index'){
                    $u = '/';
                } else {
                    $u = '/' . $this->view->controller_name . '/';
                }
            } else {
                $u = '/' . $this->view->controller_name . '/' . $this->view->action_name .'/';
            }

        } else {
            $u = $this->request->getPost('parent_url');
        }

        return $u;
    }
}