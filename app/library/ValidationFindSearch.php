<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-26
 * Time: 13:01
 */

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;

class ValidationFindSearch extends Validation
{
    public function initialize()
    {
        $this->add(
            'search-string',
            new PresenceOf(
                [
                    'message' => 'Строка поиска обязательно должна быть заполнена!',
                ]
            )
        );
    }
}