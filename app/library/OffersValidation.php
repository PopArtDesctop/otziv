<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-20
 * Time: 16:48
 */

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;

class OffersValidation extends Validation
{
    public function initialize()
    {
        $this->add(
            'description',
            new PresenceOf(
                [
                    'message' => 'Описание заказа обязательно',
                ]
            )
        );

        $this->add(
            'email',
            new PresenceOf(
                [
                    'message' => 'Электронная почта обязательна',
                ]
            )
        );

        $this->add(
            'phone',
            new PresenceOf(
                [
                    'message' => 'Телефон обязателен',
                ]
            )
        );

        $this->add(
            'cost',
            new PresenceOf(
                [
                    'message' => 'Цена обязательна',
                ]
            )
        );

        $this->add(
            'days_for_wait',
            new PresenceOf(
                [
                    'message' => 'Обязательно укажите сроки исполнения заказа',
                ]
            )
        );

        $this->add(
            'name',
            new PresenceOf(
                [
                    'message' => 'Обязательно укажите как к вам обращаться',
                ]
            )
        );
    }
}