<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-06
 * Time: 18:37
 */

use Phalcon\Session\Bag;

trait trait_Orders_Add{

    public function AddOrderIncludeFile() {

        if ($this->request->isPost()) {

            $this->view->order_name = $this->request->getPost('order_name', ['striptags', 'trim'] );
            $this->view->email =  $this->request->getPost('email', ['striptags', 'email', 'trim'] );
            $this->view->phone = $this->request->getPost('phone', ['striptags', 'int', 'trim'] );
            $this->view->region_name = $this->request->getPost('region_name', ['striptags', 'trim'] );
            $this->view->region_id = $this->request->getPost('region_id', ['striptags', 'trim'] );
            $this->view->categorie_id = (int)$this->request->getPost('categorie_id', ['striptags', 'int', 'trim'] );
            $this->view->categorie_name = $this->request->getPost('categorie_name', ['striptags', 'trim'] );

            $bag = new Bag('orders_add');
            $bag->set('order_name', $this->view->order_name);
            $bag->set('email', $this->view->email);
            $bag->set('phone', $this->view->phone);
            $bag->set('region_name', $this->view->region_name);
            $bag->set('region_id', $this->view->region_id);
            $bag->set('categorie_id', $this->view->categorie_id);
            $bag->set('categorie_name', $this->view->categorie_name);


            if ( !$this->user->isAuth() ) {

                $this->flash->error( 'Чтобы отправить заказ вы должны войти на сайт как зарегистрированный пользователь' );
                $this->response->redirect($this->view->parent_url);
                $this->view->disable();

                return;
            }

            $validation = new ValidationOrders();
            $messages = $validation->validate($_POST);

            if (count($messages)>0) {
                $er_str = '';
                foreach ($messages as $message) {
                    $er_str = $er_str . ' ' . $message;
                }
                $this->flash->error( $er_str );
                $this->response->redirect($this->view->parent_url);
                $this->view->disable();

                return;
            }

            if ( $this->request->hasFiles(true) == true ) {
                $upload_dir = APP_PATH . $this->config->upload->user_order_dir;
                $fl = null;
                foreach ($this->request->getUploadedFiles('file_pdf') as $fl) {
                    $file = $fl;
                }

                if( $file->getError() > 0 ) {
                    $this->flash->error('Ошибка загрузки файла');
                    $this->response->redirect($this->view->parent_url);
                    $this->view->disable();

                    return;
                }

                if( strtolower($file->getExtension() ) != 'pdf' AND $file->getRealType() != 'application/pdf'){
                    $this->flash->error( 'Возможна загрузка только pdf файла' );
                    $this->response->redirect($this->view->parent_url);
                    $this->view->disable();

                    return;
                }

                $dt = new \DateTime();

                $name_file = md5 ( $file->getName().$dt->getTimestamp() ) . "." .$file->getExtension();

                if( $fl->moveTo($upload_dir . $name_file )) {
                        $this->db->begin();
                        $sql = "INSERT INTO " . $this->schema
                            . ".orders_hip( order_name, email, phone, file_name, active, date_create) 
                                  VALUES ( :order_name, :email, :phone, :file_name, :active, :date_create)";

                        $s = $this->db->query($sql, ['order_name' => $this->view->order_name,
                            'email' => $this->view->email,
                            'phone' => $this->view->phone,
                            'file_name' => $name_file,
                            'active' =>1,
                            'date_create' => 'NOW'
                        ]);

                        if( $this->user->isAuth()) {
                            $id_user = $this->user->getId();
                        } else {
                            $id_user = null;
                        }

                        $lastId = $this->db->lastInsertId();

                        $ss = explode(',', $this->view->region_id );

                        foreach( $ss as $key=>$value ){
                            $sql = "INSERT INTO " . $this->schema
                                . ".link_users_orders_hip( id_users, id_orders_hip, id_cities, id_categories ) VALUES ( :id_users, :id_orders_hip, :id_cities, :id_categories )";

                            $s = $this->db->query($sql, [
                                'id_users' => $id_user,
                                'id_orders_hip' => $lastId,
                                'id_cities' => ( int )$value,
                                'id_categories' => $this->view->categorie_id
                            ]);
                        }

                        $this->db->commit();

                        $this->user->updateUserOrdersInSession( );

                        $bag->set('order_name', null);
                        $bag->set('email', null);
                        $bag->set('phone', null);
                        $bag->set('region_name', null);
                        $bag->set('region_id', null);
                        $bag->set('categorie_id', null);
                        $bag->set('categorie_name', null);

                        $this->flash->success( 'Заказ сохранен' );
                        $this->response->redirect($this->view->parent_url);
                        $this->view->disable();

                        return;

                } else {
                    $this->flash->error( 'Ошибка сохранения файла заказа' );
                    $this->response->redirect($this->view->parent_url);
                    $this->view->disable();

                    return;
                }

            } else {
                $this->flash->error( 'Вы не загрузили файл с заказом' );
                $this->response->redirect($this->view->parent_url);
                $this->view->disable();

                return;
            }

        } else {

            $this->response->redirect('/');
            $this->view->disable();
            return;
        }
    }


    public function AddOrderWithOutFile() {

        if ($this->request->isPost()) {

            $this->view->order_name = $this->request->getPost('order_name', ['striptags', 'trim'] );
            $this->view->email =  $this->request->getPost('email', ['striptags', 'email', 'trim'] );
            $this->view->phone = $this->request->getPost('phone', ['striptags', 'int', 'trim'] );
            $this->view->region_name = $this->request->getPost('region_name', ['striptags', 'trim'] );
            $this->view->region_id = $this->request->getPost('region_id', ['striptags', 'trim'] );
            $this->view->categorie_id = (int)$this->request->getPost('categorie_id', ['striptags', 'int', 'trim'] );
            $this->view->categorie_name = $this->request->getPost('categorie_name', ['striptags', 'trim'] );

            $bag = new Bag('orders_add');
            $bag->set('order_name', $this->view->order_name);
            $bag->set('email', $this->view->email);
            $bag->set('phone', $this->view->phone);
            $bag->set('region_name', $this->view->region_name);
            $bag->set('region_id', $this->view->region_id);
            $bag->set('categorie_id', $this->view->categorie_id);
            $bag->set('categorie_name', $this->view->categorie_name);


            if ( !$this->user->isAuth() ) {

                $this->flash->error( 'Чтобы отправить заказ вы должны войти на сайт как зарегистрированный пользователь' );
                $this->response->redirect($this->view->parent_url);
                $this->view->disable();

                return;
            }

            $validation = new ValidationOrders();
            $messages = $validation->validate($_POST);

            if (count($messages)>0) {
                $er_str = '';
                foreach ($messages as $message) {
                    $er_str = $er_str . ' ' . $message;
                }
                $this->flash->error( $er_str );
                $this->response->redirect($this->view->parent_url);
                $this->view->disable();

                return;
            }
                    $this->db->begin();
                    $sql = "INSERT INTO " . $this->schema
                        . ".orders_hip( order_name, email, phone, file_name, active, date_create) 
                                  VALUES ( :order_name, :email, :phone, :file_name, :active, :date_create)";

                    $s = $this->db->query($sql, ['order_name' => $this->view->order_name,
                        'email' => $this->view->email,
                        'phone' => $this->view->phone,
                        'file_name' => null,
                        'active' =>1,
                        'date_create' => 'NOW'
                    ]);

                    if( $this->user->isAuth()) {
                        $id_user = $this->user->getId();
                    } else {
                        $id_user = null;
                    }

                    $lastId = $this->db->lastInsertId();

                   $ss = explode(',', $this->view->region_id );

                   foreach( $ss as $key=>$value ){
                       $sql = "INSERT INTO " . $this->schema
                           . ".link_users_orders_hip( id_users, id_orders_hip, id_cities, id_categories ) VALUES ( :id_users, :id_orders_hip, :id_cities, :id_categories )";

                       $s = $this->db->query($sql, [
                           'id_users' => $id_user,
                           'id_orders_hip' => $lastId,
                           'id_cities' => ( int )$value,
                           'id_categories' => $this->view->categorie_id
                       ]);
                   }

                    $this->db->commit();

                    $this->user->updateUserOrdersInSession( );

                    $bag->set('order_name', null);
                    $bag->set('email', null);
                    $bag->set('phone', null);
                    $bag->set('region_name', null);
                    $bag->set('region_id', null);
                    $bag->set('categorie_id', null);
                    $bag->set('categorie_name', null);

                    $this->flash->success( 'Заказ сохранен без добавления файла' );
                    $this->response->redirect($this->view->parent_url);
                    $this->view->disable();

                    return;

        } else {

            $this->response->redirect('/');
            $this->view->disable();
            return;
        }

    }
}