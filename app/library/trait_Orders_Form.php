<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-07
 * Time: 14:41
 */

trait trait_Orders_Form{

    public function fill_Order_Form(){

        $sql = "SELECT * FROM " . $this->schema . ".cities WHERE active = true ORDER BY name ASC";
        $s = $this->db->query($sql);
        $s->setFetchMode( Phalcon\Db::FETCH_OBJ );
        $ss[0] = (object) [
            'id' => 0,
            'name' => 'Вся Россия',
            'active' => 1,
            'range' => 0
        ];
        foreach( $s->fetchAll() as $key=>$val ){
            $ss[] = $val;
        }

        $this->view->cities = $ss;

        $sql = "SELECT * FROM " . $this->schema . ".categories WHERE active= true ORDER BY name ASC";
        $s = $this->db->query($sql);
        $s->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $this->view->categories = $s->fetchAll();

        $bag = new Phalcon\Session\Bag('orders_add');

        if( is_null( $bag->get('order_name') ) OR $bag->get('order_name') == '' ){
            $this->view->order_name = '';
        } else {
            $this->view->order_name = $bag->get('order_name');
        }

        if( is_null( $bag->get('email') ) OR $bag->get('email') ==''){
            $this->view->email = $this->user->getUserProperties( 'email_user' );
        } else {
            $this->view->email = $bag->get('email');
        }

        if( is_null( $bag->get('phone') ) OR $bag->get('phone') == ''){
            $this->view->phone = $this->user->getUserProperties( 'phone' );
        } else {
            $this->view->phone = $bag->get('phone');
        }


        if( is_null( $bag->get('region_id') ) OR $bag->get('region_id') == ''){
            $this->view->region_id = '';
        } else {
            $this->view->region_id = $bag->get('region_id');
        }
        if( is_null( $bag->get('region_name') ) OR $bag->get('region_name') == ''){
            $this->view->region_name = '';
        } else {
            $this->view->region_name = $bag->get('region_name');
        }


        if( is_null( $bag->get('categorie_id') ) OR $bag->get('categorie_id') == ''){
            $this->view->categorie_id = '';
        } else {
            $this->view->categorie_id = $bag->get('categorie_id');
        }
        if( is_null( $bag->get('categorie_name') ) OR $bag->get('categorie_name') == ''){
            $this->view->categorie_name = '';
        } else {
            $this->view->categorie_name = $bag->get('categorie_name');
        }
    }
}