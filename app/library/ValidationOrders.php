<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-10-31
 * Time: 20:13
 */

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;

class ValidationOrders extends Validation
{
    public function initialize()
    {
        $this->add(
            'order_name',
            new PresenceOf(
                [
                    'message' => 'Описание заказа обязательно',
                ]
            )
        );

        $this->add(
            'email',
            new PresenceOf(
                [
                    'message' => 'Электронная почта обязательна',
                ]
            )
        );

        $this->add(
            'phone',
            new PresenceOf(
                [
                    'message' => 'Телефон обязателен',
                ]
            )
        );

        $this->add(
            'region_id',
            new PresenceOf(
                [
                    'message' => 'Регион обязательно должен быть выбран',
                ]
            )
        );
        $this->add(
            'categorie_id',
            new PresenceOf(
                [
                    'message' => 'Категория обязательно должна быть выбрана',
                ]
            )
        );
    }
}