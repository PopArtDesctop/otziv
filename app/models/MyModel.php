 <?php
        

class MyModel extends \Phalcon\Mvc\Model{

  /**
   * 
   * Очистка значения и проверка что передано значение интерпритуруемое как NULL
   * 
   * @param type $t
   * @return type
   */
    protected function clearnyl ( $t ){
        $t = trim( $t );
        if( $t == '@' ) {
           return NULL; 
        } else {
           return $t;
        } 
    }

    /**
     * 
     * Получаем строку для подстановки в rawSql
     * 
     * @return string
     */
    public function getSchemaSource(){
        return '"'.$this->getSchema ().'"."'.$this->getSource ().'"';
    }
    
       
    /**
     * 
     * Переопределение дефолтных значений сообщений валидаторов
     * 
     * @return type
     */
    public function getMessages() {
        $messages = array();
        foreach ( parent::getMessages() as $message ) {
            switch ( $message->getType() ) {
                case 'InvalidCreateAttempt':
                    $messages[] = 'Запись не может быть создана так как уже существует';
                    break;
                case 'InvalidUpdateAttempt':
                    $messages[] = 'Запись не может быть обновлена так как не существует';
                    break;
                case 'PresenceOf':
                    $messages[] = 'Поле ' . $this->getNameFromList( $message->getField() ) . ' является обязательным для заполнения';
                    break;
                default:
                    $messages[] = $message->getMessage();
                    break;
            }
        }
        return $messages;
    }
    
    
    /**
     * 
     * Вывод наименований полей на русском
     * 
     * @param type $n
     * @return type
     */
    public function getNameFromList ( $n ){
        if( isset( $this->list_name[$n] ) ){
            return $this->list_name[$n];
        } else {
            return $n;
        }
    }
    
    /**
     * 
     * Получаем название сущности для таблицы админки
     * 
     * @return string
     */
    public function Translate() {
        return $this->own_name;
    }
    
   public function initialize()
    {
        $this->useDynamicUpdate(true);
    }
    
}