<?php
/**
 * Created by PhpStorm.
 * User: avtorpc
 * Date: 2019-11-07
 * Time: 12:58
 */


use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class User extends Model {

    private static $arr =null;
    private static $instance = null;

    public static function getInstance( $session ){
        if (null === self::$instance)
        {
            self::$instance = new self();
            self::$arr = $session->get('auth');
            if ( is_null(self::$arr)) {
                $session->set( 'auth', ['name_role' => 'guest' ]);
                self::$arr = $session->get( 'auth' );
            }
        }

        return self::$instance;
    }

    private function __clone() {}


    /**
     * Перезапись параметров сессии пользователя полностью
     *
     * @param $arr
     */
    public function setAuth( $arr ){

        $this->getDI()->getSession()->set( 'auth', $arr );
        self::$arr = $this->getDI()->getSession()->get( 'auth' );
    }

    /**
     * Получить название роли пользователя
     *
     * @return mixed
     */
    public function getRole(){

        return self::$arr['name_role'];
    }

    /**
     * Получить полный список данных из сессии пользователя в виде stdObject
     *
     * @return object
     */
    public function getAllInfoUser(){

        $x = (object) [];
        foreach ( self::$arr as $key=>$val ) {
            $x->$key = $this->getUserProperties( $key );
        }
        return $x;
    }

    public function gerUserInfoArray(){

        return self::$arr;
    }

    /**
     * Получить конкретное свойство из сессии пользователя
     *
     * @param $name
     * @return string
     *
     */
    public function getUserProperties( $name ){
        if( isset( self::$arr[$name] ) ) {

            return self::$arr[$name];
        } else {

            return '';
        }
    }


    /**
     * Проверка аутентификации пользователя
     *
     * @return bool
     */
    public function isAuth(){
        if( isset( self::$arr['name_role'] ) ) {

           if( self::$arr['name_role'] == 'fizik' OR self::$arr['name_role'] == 'urik'){

               return true;
           }
        }

        return false;
    }

    /**
     * Получаем идентификатор пользователя
     *
     * @return bool
     */
    public function getId(){
        if( isset( self::$arr['id_user'] ) ) {

           return self::$arr['id_user'];
        }

        return false;
    }


    public function hashPASS( $string_pass ){
        return md5( trim ( $string_pass ) );
    }

    /**
     * Получаем список заказов пользователя
     *
     * @param $id_user
     * @return array
     */
    public function getUserOrders( $id_user ){
        $sql = "SELECT 
                               LURH.id_orders_hip AS id_orders_hip, 
                               LFOR.id_offers_hip AS id_offers_hip, 
                               LFOR.favorite AS favotite
                               FROM " . $this->getDI()->get('config')->database->schema . ".link_users_orders_hip AS LURH "
            . "LEFT JOIN " . $this->getDI()->get('config')->database->schema . ".link_offers_hip_orders_hip AS LFOR ON LURH.id_orders_hip = LFOR.id_orders_hip "
            . "WHERE id_users = " . $id_user;

        $arr = [];
        $ss = $this->getDI()->get('db')->query( $sql );
        $ss->setFetchMode( Phalcon\Db::FETCH_OBJ );
        foreach ( $ss->fetchAll() as $value ){
            if( !is_null( $value->id_offers_hip ) ) {
                $arr[$value->id_orders_hip][$value->id_offers_hip] = ( $value->favotite ) ? 1:0;
            } else {
                $arr[$value->id_orders_hip] = 0;
            }
        }

        return $arr;
    }

    /**
     * Получаем список откликов пользователя
     *
     * @param $id_user
     * @return array
     */
    public function getUserOffers( $id_user ){
        $sql = "SELECT 
                                  LFOR.id_orders_hip AS id_orders_hip, 
                                  LUFH.id_offers_hip AS id_offers_hip,
                                  LFOR.favorite AS favotite
                                  FROM " . $this->getDI()->get('config')->database->schema . ".link_users_offers_hip AS LUFH "
            . "LEFT JOIN " . $this->getDI()->get('config')->database->schema . ".link_offers_hip_orders_hip AS LFOR ON LFOR.id_offers_hip = LUFH.id_offers_hip "
            . "WHERE LUFH.id_users = " . $id_user;

        $ss = $this->getDI()->get('db')->query( $sql );
        $ss->setFetchMode( Phalcon\Db::FETCH_OBJ );
        $arr = [];
        foreach ( $ss->fetchAll() as $value ){
            $arr[$value->id_offers_hip] = $value->favotite;
        }

        return $arr;
    }

    /**
     *  Проверяем что заказ принадлежит пользователю
     *
     * @param $id_order
     * @return bool
     */
    public function validUserOwnerOrderID( $id_order ){
        if( isset( self::$arr['orders'] )) {
            if (array_key_exists( $id_order, self::$arr['orders'] )) {

                return true;
            } else {

                return false;
            }
        }
    }

    /**
     * Проверяем что отклик принадлежит пользователю
     *
     * @param $id_offer
     * @return bool
     */
    public function validUserOwnerOfferID( $id_offer ){
        if( isset( self::$arr['offers']) ) {
            if (array_key_exists( $id_offer, self::$arr['offers'] )) {

                return true;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }


    /**
     * Обновляем заказы пользователя хранящиеся в сессии
     */
    public function updateUserOrdersInSession( ){
        self::$arr['orders'] = $this->getUserOrders( $this->getId() );
        $this->getDI()->getSession()->set( 'auth', self::$arr );
    }

    /**
     *  Обновляем отклики пользователя хранящиеся в сессии
     */
    public function updateUserOffersInSession( ) {
        self::$arr['offers'] = $this->getUserOffers( $this->getId() );
        $this->getDI()->getSession()->set( 'auth', self::$arr );
    }

    /**
     * Проверка есть ли на данный заказ выбранный отклик
     *
     * @param $id_order
     * @return bool
     */
    public function validUserOrderFavorite( $id_order ) {
        if( isset( self::$arr['orders'][$id_order] )) {
            echo "<pre>"; print_r( self::$arr['orders'][$id_order] );
           foreach ( self::$arr['orders'][$id_order] as &$value ) {
               if( $value == 1 ) {
                   return true;
               }
            }
        }

        return false;
    }


    /**
     * Проверка есть ли на этот заказ уже выбранный фаворитом отклик
     *
     * @param $id_order
     */
    public function validOrderFavorite( $id_order ){
        $sql = "SELECT count( id ) FROM " .  $this->getDI()->get('config')->database->schema . ".link_offers_hip_orders_hip WHERE id_orders_hip=:id_order AND favorite= 'true'";

        $s = $this->getDI()->get('db')->query($sql, ['id_order' => $id_order ]);
        $s->setFetchMode( Phalcon\Db::FETCH_OBJ );
        $s = $s->fetchAll();

        if( $s[0]->count > 0 ){

            return true;
        } else {

            return false;
        }

    }

}