<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

    use trait_Parent_Url;

    protected $shema;
    protected $user;

    protected function initialize()
    {
        $this->tag->prependTitle('Отзывы | ');
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

    }
}
