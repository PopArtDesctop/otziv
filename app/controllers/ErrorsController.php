<?php

/**
 * ErrorsController
 *
 * Manage errors
 */
class ErrorsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Ошибка!');

        parent::initialize();
    }

    public function show404Action()
    {
        
        die( '404');
       }

    public function show401Action()
    {
        die( '401');
      }

    public function show500Action()
    {
        die( '500');
       }
}
