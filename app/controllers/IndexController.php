<?php

use Phalcon\Mvc\Model;
use Phalcon\Session\Bag;

class IndexController extends ControllerBase {

    public function initialize()
    {
        $this->tag->setTitle('Главная');

        parent::initialize();

    }

    public function indexAction(){

        $sql = "SELECT * FROM " . $this->schema . ".messages";

        $s = $this->db->query($sql );
        
        if( $s->numRows() ==0 ) {
              $this->view->list_messages = null;
        } else {
            $s->setFetchMode( Phalcon\Db::FETCH_OBJ );
            $this->view->list_messages = $s->fetchAll();
          //  echo "<pre>"; print_r( $this->view->list_messages ); die;
        }    
    }
    
    
    public function messageAction(){
        
        
        
        $d_t = new DateTime();
        
        $sql = "INSERT INTO " . $this->schema . ".messages( d_t, user_name, user_mess) VALUES (:d_t, :user_name, :user_mess)";
       
        $s = $this->db->query($sql, [
                ':d_t' => $d_t->format("Y-m-d H:i:s"),
                ':user_name' => trim( base64_decode( $this->request->getPost('name', ['string', 'striptags']) ) ),
                ':user_mess' =>trim( base64_decode( $this->request->getPost('comment', ['string', 'striptags']) ) )
        ]);
        
        $sql = "SELECT * FROM " . $this->schema . ".messages WHERE id= ". $this->db->lastInsertId();
        $s = $this->db->query($sql );
        $s->setFetchMode( Phalcon\Db::FETCH_OBJ );
        $this->view->list_messages = $s->fetchAll();
       // echo "<pre>"; print_r( $this->view->list_messages ); die;
        }
          

}
