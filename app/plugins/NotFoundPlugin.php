<?php

use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Dispatcher;
use Phalcon\Mvc\Dispatcher\Exception as DispatcherException;
use Phalcon\Mvc\Dispatcher as MvcDispatcher;

/**
 * NotFoundPlugin
 *
 * Handles not-found controller/actions
 */
class NotFoundPlugin extends Plugin
{
    /**
     * This action is executed before perform any action in the application
     *
     * @param Event $event
     * @param MvcDispatcher $dispatcher
     * @param \Exception $exception
     * @return boolean
     */
   public function beforeException(Event $event, MvcDispatcher $dispatcher, \Exception $exception)
    {
       
       echo $exception->getMessage(); die;
       
        error_log(
            $exception->getMessage() . PHP_EOL . $exception->getTraceAsString() . PHP_EOL, 3, APP_PATH . $this->config->logger->error
        );

        if( stristr($exception->getMessage(), 'SQLSTATE') !== FALSE) {
            $this->db->rollback();
        }

        if ($exception instanceof DispatcherException) {
            switch ($exception->getCode()) {
                case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward(
                        [
                            'controller' => 'errors',
                            'action'     => 'show404',
                        ]
                    );
                    return false;
            }
        }


        if( $exception->getCode() === 404 OR $exception->getCode() == '404' ){
            $dispatcher->forward(
                [
                    'controller' => 'errors',
                    'action'     => 'show404',
                ]
            );
            return false;
        }

        if( $exception->getCode() === 401 OR $exception->getCode() == '401' ){
            $dispatcher->forward(
                [
                    'controller' => 'errors',
                    'action'     => 'show401',
                ]
            );
            return false;
        }


            $dispatcher->forward(
                [
                    'controller' => 'errors',
                    'action'     => 'show500',
                ]
            );
            return false;

    }
}
