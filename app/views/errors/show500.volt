<!DOCTYPE html>
<html>
<head>
    <title>500</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <link href="/libs/normalize.css" rel="stylesheet">
    <link href="/fonts/PFBeauSansPro/stylesheet.css" rel="stylesheet">
    <link href="/libs/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/libs/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/libs/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.css" rel="stylesheet">
    <link href="/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</head>
<body class="error-404">

<header class="header --bg2">
    <div class="container">
        <a href="/" class="logo"><img class="img-responsive" src="/img/logo.png"></a>
    </div>
</header>
<section class="section">
    <div class="container">
        <div class="error-404__image">
            <img class="img-responsive" src="/img/err_500.png">
        </div>
        <p>Извините, произошла системная ошибка.</p>
        <a href="#">Перейти на главную страницу.</a>
    </div>
</section>
<footer class="footer --bg2">
    <div class="container">
        <div class="copyright">© Платформа для бизнеса</div>
    </div>
</footer>

<script src="/libs/jquery.min.js" type="text/javascript"></script>
<script src="/libs/OwlCarousel2-2.3.4/dist/owl.carousel.min.js" type="text/javascript"></script>
<script src="/libs/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/functions.js" type="text/javascript"></script>
<script src="/js/common.js" type="text/javascript"></script>

</body>
</html>