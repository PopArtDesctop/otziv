<!DOCTYPE html>
<html>
<head>
    {{ get_title() }}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <link href="/css/materialize.min.css" rel="stylesheet">
    <link href="/css/style.css?{{ elements.get_timer() }}" rel="stylesheet">
    
    <script src="/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="/js/materialize.min.js" type="text/javascript"></script>
    <script src="/js/own.js?{{ elements.get_timer() }}" type="text/javascript"></script>
    
</head>
<body>

{% block mn%}
{% endblock %}

</body>
</html>
