{% extends "index.volt" %}

{% block mn%}
    <div class="container">
        <div class="row" id="header-container">
            <div class="col s12 m6 l8" id="header-contacts">
                <br/>
                Телефон: (499) 340-94-71<br/>
                Email: info@future-group.ru
                <br/>
                <h1>Комментарии</h1>     
            </div>
            <div class="col s12 m6 l4" id="logo-container">
                <img src="/img/logo.png" title="logo" class="logo">
        </div>
        </div>
        <div class="row zub">
            <div class="col s12 m12 s12">
                <div class="row" id="comments">
                    {% if list_messages is empty %}
                    <div class="col s6 m4 l2" id="empty-messages">
                        Нет сообщений
                    </div>
                    {%  else %}    
                        {%  for key, item in list_messages %}
                        {{ partial("layouts/common_messages" ) }}
                        {% endfor %}
                    {% endif %}
                </div> 
                <div class="row" id="block-comment">
                    <div class="col s12 m12 l12 zagolovok-for-block-comment">
                        Оставить комментарий
                    </div>
                    <div class="col s12 m12 l12 name-for-block-comment">
                       Ваше имя
                    </div>
                    <div class="col s12 m12 l12 input-name-for-block-comment">
                       <input type="text" name="name" id ="input-name-for-block-comment">
                    </div>
                    <div class="col s12 m12 l12 zagolovok-comment-for-block-comment">
                       Ваш комментарий
                    </div>
                    <div class="col s12 m12 l12 input-comment-for-block-comment">
                       <textarea id="comment-area" name="comment">
                       </textarea>
                    </div>
                    <div class="col s12 m12 l12 send-comment-for-block-comment">
                       <button id="send-commit">
                           Отправить
                       </button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
{% endblock %}

