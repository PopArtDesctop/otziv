<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

///echo "<pre>"; print_r( $_REQUEST ); die;

//phpinfo(); die;

use Phalcon\Mvc\Application;
use Phalcon\Config\Adapter\Ini as ConfigIni;

try {
    define(
        'APP_PATH',
        realpath('..') . '/'
    );



    /**
     * Read the configuration
     */
    $config = new ConfigIni(APP_PATH . 'app/config/config.ini');

    if (is_readable(APP_PATH . 'app/config/config.ini.dev')) {
        $override = new ConfigIni(
            APP_PATH . 'app/config/config.ini.dev'
        );

        $config->merge($override);
    }

//echo "ssdlsldsld";

    /**
     * Auto-loader configuration
     */
    require APP_PATH . 'app/config/loader.php';

    $application = new Application(
        new Services($config)
    );


    // NGINX - PHP-FPM already set PATH_INFO variable to handle route
    $response = $application->handle(!empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : null);

    $response->send();
} catch (Exception $e){
    $d = new DateTime( 'now' );
    $d = $d->format( 'Y-m-d H:i:s' ); die;
    print_r( $e->getMessage(), true );
    ob_end_clean();
   // file_put_contents( APP_PATH . 'logs/index_excaption.log', $d . '\r\n' . print_r( $e->getMessage(), true ) . "\r\n" . print_r( $e->getTraceAsString(), true ), FILE_APPEND );
    //chmod(APP_PATH . 'logs/index_excaption.log', 0777);
    echo file_get_contents( APP_PATH . 'mp.codedone.io/zero.html' );
}
