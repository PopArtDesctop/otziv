$(document).ready(function(){
   $( '#send-commit' ).on( 'click', function() {
       let nm = utf8_to_b64( $( '#input-name-for-block-comment').val() );
       let cm = utf8_to_b64( $( '#comment-area').val() );
       send_ajax( '/index/message/', 'name=' + nm + '&comment=' + cm, false, append_data_to_comments )
   })
})


function send_ajax( url, data_to_send, synchron, func ){
    $.ajax({
        async: synchron,
        url: url,
        cache: false,
        method: "POST",
        data: data_to_send,
        success: function (data) {
            console.log( data );
          append_data_to_comments ( data );
        }
    });
};

function append_data_to_comments ( data ){
    $( '#empty-messages' ).remove();
     $("#comments").append(data);
     $( '#input-name-for-block-comment').val('');
     $( '#comment-area').val(''); 
}

function utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}