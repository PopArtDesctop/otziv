--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2
-- Dumped by pg_dump version 12.3

-- Started on 2020-09-06 00:02:30

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 25024)
-- Name: dev; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA dev;


ALTER SCHEMA dev OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 204 (class 1259 OID 25027)
-- Name: messages; Type: TABLE; Schema: dev; Owner: avtorpc
--

CREATE TABLE dev.messages (
    id integer NOT NULL,
    d_t timestamp with time zone NOT NULL,
    user_name character(128) NOT NULL,
    user_mess text NOT NULL
);


ALTER TABLE dev.messages OWNER TO avtorpc;

--
-- TOC entry 203 (class 1259 OID 25025)
-- Name: messages_id_seq; Type: SEQUENCE; Schema: dev; Owner: avtorpc
--

CREATE SEQUENCE dev.messages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dev.messages_id_seq OWNER TO avtorpc;

--
-- TOC entry 2871 (class 0 OID 0)
-- Dependencies: 203
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: dev; Owner: avtorpc
--

ALTER SEQUENCE dev.messages_id_seq OWNED BY dev.messages.id;


--
-- TOC entry 2737 (class 2604 OID 25030)
-- Name: messages id; Type: DEFAULT; Schema: dev; Owner: avtorpc
--

ALTER TABLE ONLY dev.messages ALTER COLUMN id SET DEFAULT nextval('dev.messages_id_seq'::regclass);


--
-- TOC entry 2739 (class 2606 OID 25032)
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: dev; Owner: avtorpc
--

ALTER TABLE ONLY dev.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


-- Completed on 2020-09-06 00:02:30

--
-- PostgreSQL database dump complete
--

